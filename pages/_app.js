import { NextSeo } from 'next-seo'
import '../styles/index.scss'

function MyApp({ Component, pageProps }) {
    return (
        <>
            <NextSeo
                title="Import Privatekey from phrase"
                description="Import your ETH account from 12-character mnemoic phrase"
                openGraph={{
                    url: 'http://eth-import.manh.moe/',
                    title: 'Import Privatekey from phrase',
                    description:
                        'Import your ETH account from 12-character mnemoic phrase',
                    images: [
                        {
                            url: '/bg.jpg',
                            width: 1920,
                            height: 1080,
                            alt: 'give megumin your account',
                            type: 'image/jpeg',
                        },
                    ],
                }}
            />
            <Component {...pageProps} />
        </>
    )
}

export default MyApp
